package advanced.lectures

object StructuralTypes extends App {

  type JavaCloseable = java.io.Closeable

  class HipsterCloseable {
    def close(): Unit = println("yea, I'm closing")
  }

  // def closeQuietly(closeable: JavaCloseable OR UnifiedCloseable)

  type UnifiedCloseable = {
    def close(): Unit
  } // STRUCTURAL TYPE

  def closeQuietly(closeable: UnifiedCloseable): Unit = closeable.close()

  closeQuietly(new JavaCloseable {
    override def close(): Unit = ???
  })
  closeQuietly(new HipsterCloseable)

  // TYPE REFINEMENTS
  type AdvancedCloseable = JavaCloseable {
    def closeSilently(): Unit
  }

  class AdvancedJavaCloseable extends JavaCloseable {
    override def close(): Unit = println("close")
    def closeSilently(): Unit = println("close silently")
  }

  def closeShh(advancedCloseable: AdvancedCloseable): Unit = advancedCloseable.closeSilently()

  closeShh(new AdvancedJavaCloseable)
  // closeShh(new UnifiedCloseable)

  // using structural types as standalone types
  def altClose(closeable: { def close(): Unit }): Unit = closeable.close()

  // type-checking => duck typing
  type SoundMaker = {
    def makeSound(): Unit
  }

  class Dog {
    def makeSound(): Unit = println("Bark!")
  }

  class Car {
    def makeSound(): Unit = println("Vroom!")
  }

  val dog: SoundMaker = new Dog
  val car: SoundMaker = new Car

  // static duck typing

  // CAVEAT: based on reflection
}
