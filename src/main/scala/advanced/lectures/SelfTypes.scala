package advanced.lectures

object SelfTypes extends App {

  trait Instrumentalist {
    def play(): Unit
  }

  trait Singer { this: Instrumentalist => // SELF TYPE - whoever implements Singer also needs to implement Instrumentalist

    def sing(): Unit
  }

  class LeadSinger extends Singer with Instrumentalist {
    override def sing(): Unit = ???
    override def play(): Unit = ???
  }

  /*
  class Vocalist extends Singer {
    override def sing(): Unit = ???
  }
  */

  // vs inheritance
  class A
  class B extends A // B IS AN A

  trait T
  trait S { self: T => } // S REQUIRES a T

  // cyclical dependencies

  // class X extends Y
  // class Y extends X

  trait X { self: Y => }
  trait Y { self: X => }
}
