package advanced.lectures

object OrganizingImplicits extends App {

  implicit def reverseOrdering: Ordering[Int] = Ordering.fromLessThan(_ > _)
  // implicit def normalOrdering: Ordering[Int] = Ordering.fromLessThan(_ < _)

  println(List(1, 4, 5, 3, 2).sorted)

  // scala.Predef

  /*
    Implicits:
      - val/var
      - object
      - accessor methods = defs with no parameters
   */
}
