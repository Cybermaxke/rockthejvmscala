package advanced.lectures

object FBoundedPolymorphism extends App {

  // Solution 1

  /*
  trait Animal[A <:  Animal[A]] {
    def breed: List[Animal[A]]
  }

  class Cat extends Animal[Cat] {
    override def breed: List[Animal[Cat]] = ???
  }

  class Dog extends Animal[Dog] {
    override def breed: List[Animal[Dog]] = ???
  }

  trait Entity[E <: Entity[E]] // ORM
  class Person extends Comparable[Person] { // FBP
    override def compareTo(o: Person): Int = ???
  }

  class Crocodile extends Animal[Dog] {
    override def breed: List[Animal[Dog]] = ??? // List[Dog] !!
  }
  */

  // Solution 2

  /*
  trait Animal[A <:  Animal[A]] { self: A =>
    def breed: List[Animal[A]]
  }

  class Cat extends Animal[Cat] {
    override def breed: List[Animal[Cat]] = ???
  }

  class Dog extends Animal[Dog] {
    override def breed: List[Animal[Dog]] = ???
  }

  trait Entity[E <: Entity[E]] // ORM
  class Person extends Comparable[Person] { // FBP
    override def compareTo(o: Person): Int = ???
  }

  /*
  class Crocodile extends Animal[Dog] {
    override def breed: List[Animal[Dog]] = ??? // List[Dog] !!
  }
  */

  class Crocodile extends Animal[Crocodile] {
    override def breed: List[Animal[Crocodile]] = ???
  }

  trait Fish extends Animal[Fish]
  class Shark extends Fish {
    override def breed: List[Animal[Fish]] = List(new Cod) // wrong
  }
  class Cod extends Fish {
    override def breed: List[Animal[Fish]] = ???
  }
  */

  // Exercise

  // Solution 3: type classes!

  trait Animal
  trait CanBreed[A] {
    def breed(a: A): List[A]
  }

  class Dog extends Animal
  object Dog {
    implicit object DogsCanBreed extends CanBreed[Dog] {
      override def breed(a: Dog): List[Dog] = List(new Dog)
    }
  }

  implicit class CanBreedEnrichment[A](val animal: A) extends AnyVal {
    def breed(implicit canBreed: CanBreed[A]): List[A] =
      canBreed.breed(animal)
  }

  val dog = new Dog
  dog.breed
}
