package advanced.lectures

object PimpMyLibrary extends App {

  // 2.isPrime

  implicit class RichInt(val value: Int) {
    def isEven: Boolean = value % 2 == 0
    def sqrt: Double = Math.sqrt(value)
  }

  implicit class RicherInt(value: RichInt) {
    def isOdd: Boolean = value.value % 2 != 0
  }

  new RichInt(42).sqrt

  42.isEven
  // type enrichment = pimping

  1 to 10

  import scala.concurrent.duration._
  10.seconds

  // compiler doesn't compile multiple implicit searches
  // 42.isOdd
}
