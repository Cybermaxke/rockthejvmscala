package advanced.exercises

class AdvancedPatternMatching {

  val n: Int = 45
  val mathProperty = n match {
    case x if x < 10 => "single digit"
    case x if x % 2 == 0 => "an even number"
    case _ => "no property"
  }

  // --> a more elegant solution

  object SingleDigit {
    /*
    def unapply(x: Int): Option[Int] =
      if (x < 10) Some(x)
      else None
     */

    def unapply(x: Int): Boolean = x < 10
  }

  object EvenNumber {
    /*
    def unapply(x: Int): Option[Int] =
      if (x % 2 == 0) Some(x)
      else None
     */

    def unapply(x: Int): Boolean = x % 2 == 0
  }

  /*
  val elegantMathProperty = n match {
    case SingleDigit(_) => "single digit"
    case EvenNumber(_) => "an even number"
    case _ => "no property"
  }
  */

  val elegantMathProperty = n match {
    case SingleDigit() => "single digit"
    case EvenNumber() => "an even number"
    case _ => "no property"
  }
}
