package advanced.exercises

import scala.io.Source

object PartialFunctions extends App {

  /*
    1. construct a PF instance (anonymous class)
    2. dumb chatbot as a PF
   */

  // 1.
  val partialFunction = new PartialFunction[Int, Int] {
    override def isDefinedAt(x: Int): Boolean = x == 1 || x == 2
    override def apply(v1: Int): Int = {
      if (v1 == 1) 43
      else if (v1 == 2) 55
      else throw new IllegalArgumentException
    }
  }

  // 2.

  val responseFunction: PartialFunction[String, String] = {
    case "hello" => "Hello!"
    case "wakeup" =>  "I'm already awake?"
  }

  val lifted = responseFunction.lift

  Source.stdin.getLines().map(_.toLowerCase).map(lifted).foreach { response =>
    println("> " + response.getOrElse("I don't know how to answer to this."))
  }
}
