package advanced.exercises

object TypeMembers extends App {

  // enforce a type to be applicable to some types only

  // LOCKED
  trait MList {
    type A
    def head: A
    def tail: MList
  }

  trait ApplicableToNumbers {
    type A <: Number
  }

  // NOT OK
  /*
  class CustomList(hd: String, tl: CustomList) extends MList with ApplicableToNumbers {
    override type A = String
    override def head: String = hd
    override def tail: MList = tl
  }
  */

  // OK
  /*
  class IntList(hd: Int, tl: IntList) extends MList with ApplicableToNumbers {
    override type A = Int
    override def head: Int = hd
    override def tail: MList = tl
  }
  */
}
