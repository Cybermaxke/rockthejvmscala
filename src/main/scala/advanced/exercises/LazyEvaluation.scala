package advanced.exercises

import advanced.exercises.LazyEvaluation.{EmptyStream, MyStream, NonEmptyStream}

object LazyEvaluation extends App {

  /*
    implement a lazily evaluated, singly linked stream of elements

    naturals = MyStream.from(1)(x => x + 1) = stream of natural numbers (potentially infinite)
    naturals.take(100).foreach(println) // lazily evaluated stream of the first 100 naturals
    naturals.foreach(println) // will crash
    naturals.map(_ * 2) // stream of all even numbers
   */

  trait MyStream[+A] {
    def isEmpty: Boolean
    def head: A
    def tail: MyStream[A]

    def #::[B >: A](element: B): MyStream[B] // prepend operator
    def ++[B >: A](stream: => MyStream[B]): MyStream[B] // concatenate two streams

    def foreach(f: A => Unit)
    def map[B](f: A => B): MyStream[B]
    def flatMap[B](f: A => MyStream[B]): MyStream[B]
    def filter(predicate: A => Boolean): MyStream[A]

    def take(n: Int): MyStream[A]
    def takeAsList(n: Int): List[A] = take(n).toList

    def toList: List[A]
  }

  object EmptyStream extends MyStream[Nothing] {
    override def isEmpty: Boolean = true
    override def head: Nothing = throw new NoSuchElementException
    override def tail: MyStream[Nothing] = throw new NoSuchElementException
    override def #::[B >: Nothing](element: B): MyStream[B] = new NonEmptyStream[B](element, this)
    override def ++[B >: Nothing](stream: => MyStream[B]): MyStream[B] = stream
    override def foreach(f: Nothing => Unit): Unit = ()
    override def map[B](f: Nothing => B): MyStream[B] = this
    override def flatMap[B](f: Nothing => MyStream[B]): MyStream[B] = this
    override def filter(predicate: Nothing => Boolean): MyStream[Nothing] = this
    override def take(n: Int): MyStream[Nothing] = this
    override def toList: List[Nothing] = Nil
  }

  class NonEmptyStream[A](h: A, t: => MyStream[A]) extends MyStream[A] {
    override def isEmpty: Boolean = false

    override val head: A = h
    override lazy val tail: MyStream[A] = t

    override def #::[B >: A](element: B): MyStream[B] =
      new NonEmptyStream[B](element, this)

    override def ++[B >: A](stream: => MyStream[B]): MyStream[B] =
      new NonEmptyStream[B](head, tail ++ stream)

    override def foreach(f: A => Unit): Unit = {
      f(head)
      tail.foreach(f)
    }

    override def map[B](f: A => B): MyStream[B] =
      new NonEmptyStream[B](f(head), tail.map(f))

    override def flatMap[B](f: A => MyStream[B]): MyStream[B] =
      f(head) ++ tail.flatMap(f)

    override def filter(predicate: A => Boolean): MyStream[A] =
      if (predicate(head)) new NonEmptyStream[A](head, tail.filter(predicate))
      else tail.filter(predicate)

    override def take(n: Int): MyStream[A] =
      if (n <= 0) EmptyStream
      else if (n == 1) new NonEmptyStream[A](head, EmptyStream)
      else new NonEmptyStream[A](head, tail.take(n - 1))

    override def toList: List[A] = List(head) ++ tail.toList
  }

  object MyStream {
    def from[A](start: A)(generator: A => A): MyStream[A] =
      new NonEmptyStream[A](start, MyStream.from(generator(start))(generator))
  }
}

object StreamsPlayground extends App {
  val naturals = MyStream.from(1)(_ + 1)
  println(naturals.head)
  println(naturals.tail.head)
  println(naturals.tail.tail.head)

  val startFrom0 = 0 #:: naturals
  println(startFrom0.head)

  println(startFrom0.takeAsList(100))

  // map, flatMap
  println(startFrom0.map(_ * 2).take(100).toList)
  println(startFrom0.flatMap(x => new NonEmptyStream(x, new NonEmptyStream(x + 1, EmptyStream))).take(10).toList)
  println(startFrom0.filter(_ < 10).take(10).toList)

  // Exercises on streams
  // 1. stream of Fibonacci numbers (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...)
  //    f(1) = 1
  //    f(2) = 1
  //    f(n) = f(n - 1) + f(n - 2)

  /*
  val fibonacci = {
    def f(n: Int): Int = n match {
      case 0 => 0
      case 1 => 1
      case 2 => 1
      case _ => f(n - 1) + f(n - 2)
    }
    def fStream(n: Int): MyStream[Int] =
      new NonEmptyStream[Int](f(n), fStream(n + 1))
    fStream(1)
  }

  println(fibonacci.take(10).toList)
  */

  /*
    [ first, [ ...
    [ first, fibo(second, first + second)
   */
  def fibonacci(first: Int, second: Int): MyStream[Int] =
    new NonEmptyStream[Int](first, fibonacci(second, first + second))

  println(fibonacci(1, 1).take(100).toList)

  // 2. stream of prime numbers with Eratosthenes' sieve
  // [2 3 4 ... ]
  // filter out all numbers divisible by 2
  // [2 3 5 7 9 11 ... ]
  // filter out all numbers divisible by 3
  // [2 3 5 7 11 13 17 ... ]
  // filter out all numbers divisible by 5

  /*
    [ 2 3 4 5 6 7 8 9 10 11 12 ... ]
    [ 2 3 5 7 9 11 13 ... ]
    [ 2 eratosthenes applied to (numbers filtered by n % 2 != 0)
    [ 2 3 eratosthenes applied to [ 5 7 9 11 ... ] applied to (numbers filtered by n % 3 != 0)
    [ 2 3 5
   */
  def eratosthenes(numbers: MyStream[Int]): MyStream[Int] =
    if (numbers.isEmpty) numbers
    else new NonEmptyStream[Int](numbers.head, eratosthenes(numbers.tail.filter(n => n % numbers.head != 0)))

  println(eratosthenes(MyStream.from(2)(_ + 1)).take(100).toList)
}