package advanced.exercises

object ConcurrencyProblems extends App {

  /*
    1. Construct 50 "inception" threads,
         Thread1 -> thread2 -> thread3 -> ...
         println("hello from thread #3")
       in REVERSE ORDER
   */

  def inceptionThreads(maxThreads: Int, i: Int = 1): Thread = new Thread(() => {
    if (i < maxThreads) {
      val newThread = inceptionThreads(maxThreads, i + 1)
      newThread.start()
      newThread.join()
    }
    println(s"Hello from thread #$i")
  })

  inceptionThreads(50).start()

  /*
    2.
   */
  var x = 0
  var threads = (1 to 100).map(_ => new Thread(() => x += 1))
  threads.foreach(_.start())
  /*
    1) What is the biggest value possible for x? 100
    2) What is the smallest value possible for x? 1
   */

  /*
    3. Sleep fallacy
   */

  var message = ""
  val awesomeThread = new Thread(() => {
    Thread.sleep(1000)
    message = "Scala is awesome"
  })

  message = "Scala sucks"
  awesomeThread.start()
  Thread.sleep(1001)
  println(message)
  /*
    what's the value of message? almost always "Scala is awesome"
    is it guaranteed? NO!
    why? why not?

    (main thread)
      message = "Scala sucks"
      awesomeThread.start()
      sleep() - relieves execution
    (awesome thread)
      sleep() - relieves execution
    (OS gives the CPU to some important thread - takes CPU for more than 2 seconds)
    (OS gives the CPU back to MAIN thread)
      println("Scala sucks")
    (OS gives the CPU to awesomeThread)
      println("Scala is awesome")
   */
}
