package advanced.exercises

object Monads extends App {

  /*
    1. implement a Lazy[T] monad = computation which will only be executed when it's needed.

    unit/apply
    flatMap
   */

  class Lazy[+A](value: => A) {
    private lazy val internal = value
    def get: A = internal
    def flatMap[B](f: A => Lazy[B]): Lazy[B] = new Lazy[B](f(internal).get)
  }

  object Lazy {
    def apply[A](value: => A): Lazy[A] =
      new Lazy[A](value)
  }

  val lazyInstance = Lazy {
    println("Lazy")
    42
  }

  // println(lazyInstance.get)

  val flatMappedInstance = lazyInstance.flatMap { x =>
    Lazy {
      x * 10
    }
  }

  // println(flatMappedInstance.get)

  /*
    2. Monads = unit + flatMap
       Monads = unit + map + flatten

       Monad[T] {

         def flatMap[B](f: T => Monad[B]): Monad[B] = ... (implemented)

         def map[B](f: T => B): Monad[B] = flatMap(x => unit(f(x)))
         def flatten(m: Monad[Monad[T]]): Monad[T] = m.flatMap((x: Monad[T]) => x)
       }
   */


}
