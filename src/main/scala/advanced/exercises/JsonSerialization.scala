package advanced.exercises

import java.util.Date

object JsonSerialization extends App {

  case class User(name: String, age: Int, email: String)
  case class Post(content: String, createdAt: Date)
  case class Feed(user: User, posts: List[Post])

  /*
    1 - intermediate data types: Int, String, List, Date
    2 - type classes for conversion to intermediate data types
    3 - serialize to json
   */

  sealed trait JsonValue { // intermediate data type
    def stringify: String
  }

  final case class JsonString(value: String) extends JsonValue {
    override def stringify: String = "\"" + value.replace("\"", "\\\"") + "\""
  }

  final case class JsonNumber(value: Int) extends JsonValue {
    override def stringify: String = value.toString
  }

  final case class JsonObject(values: Map[String, JsonValue]) extends JsonValue {
    override def stringify: String = values.map {
      case (key, value) => "\"" + key + "\":" + value.stringify
    }.mkString("{", ",", "}")
  }

  object JsonObject {
    def apply(values: (String, JsonValue)*): JsonObject = new JsonObject(values.toMap)
  }

  final case class JsonArray(values: List[JsonValue]) extends JsonValue {
    override def stringify: String = values.map(_.stringify).mkString("[", ",", "]")
  }

  object JsonArray {
    def apply(values: JsonValue*): JsonArray =
      JsonArray(values.toList)
  }

  val data = JsonObject(
    "user" -> JsonString("Daniel"),
    "posts" -> JsonArray(
      JsonString("Scala Rocks!"),
      JsonNumber(453)
    )
  )

  println(data.stringify)

  // type class

  trait JsonConverter[T] {
    def convert(value: T): JsonValue
  }

  implicit class JsonEnrichment[T](val value: T) extends AnyVal {
    def toJson(implicit converter: JsonConverter[T]): JsonValue =
      converter.convert(value)
  }

  implicit object StringConverter extends JsonConverter[String] {
    override def convert(value: String): JsonValue = JsonString(value)
  }

  implicit object NumberConverter extends JsonConverter[Int] {
    override def convert(value: Int): JsonValue = JsonNumber(value)
  }

  implicit object DateConverter extends JsonConverter[Date] {
    override def convert(value: Date): JsonValue = JsonString(value.toString)
  }

  implicit def listConverter[T](implicit converter: JsonConverter[T]): JsonConverter[List[T]] =
    (value: List[T]) => JsonArray(value.map(converter.convert))

  /*
  // Not working...
  implicit class ListConverter[T](implicit converter: JsonConverter[T]) extends JsonConverter[List[T]] {
    override def convert(value: List[T]): JsonValue = JsonArray(value.map(converter.convert))
  }
  */

  implicit object UserConverter extends JsonConverter[User] {
    override def convert(value: User): JsonValue = JsonObject(
      "name" -> value.name.toJson,
      "age" -> value.age.toJson,
      "email" -> value.email.toJson
    )
  }

  implicit object PostConverter extends JsonConverter[Post] {
    override def convert(value: Post): JsonValue = JsonObject(
      "content" -> value.content.toJson,
      "created" -> value.createdAt.toJson
    )
  }

  implicit object FeedConverter extends JsonConverter[Feed] {
    override def convert(value: Feed): JsonValue = JsonObject(
      "user" -> value.user.toJson,
      "posts" -> value.posts.toJson
    )
  }

  // call stringify on result

  val now = new Date()
  val jeff = User("Jeff", 23, "jeff@hotmail.com")

  val feed = Feed(jeff, List(
    Post("Hello", now),
    Post("Look at this cute puppy", now)
  ))

  println(feed.toJson.stringify)
}
