package advanced.exercises

object PimpMyLibrary extends App {

  /*
    Enrich the String class
    - asInt
    - encrypt
      "John" -> Lnjp

     Enrich the Int class
     - times(function)
       3.times(() => ...)
     - *
       3 * List(1, 2) => List(1, 2, 1, 2, 1, 2)
   */

  implicit class RichString(string: String) {

    def asInt: Int =
      Integer.parseInt(string)

    def encrypt(distance: Int): String =
      string.map(c => (c + distance).toChar)
  }

  implicit class RichInt(val value: Int) extends AnyVal {

    def times(f: () => Unit): Unit =
      (0 until value).foreach(_ => f())

    def *[A](list: List[A]): List[A] = {
      def concat(n: Int): List[A] =
        if (n <= 0) Nil
        else concat(n - 1) ++ list
      concat(value)
    }
  }

  3.times(() => println("Scala Rocks!"))
  println(4 * List(1, 2))

  implicit def stringToInt(string: String): Int = Integer.parseInt(string)
  println("6" / 2)
}
