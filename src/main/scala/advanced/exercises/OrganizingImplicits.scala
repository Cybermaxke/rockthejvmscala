package advanced.exercises

object OrganizingImplicits extends App {

  case class Person(name: String, age: Int)

  val persons = List(
    Person("Steve", 30),
    Person("Amy", 22),
    Person("John", 66)
  )

  // implicit def nameOrdering: Ordering[Person] = Ordering.by(_.name)

  object NameOrdering {

    implicit def nameOrdering: Ordering[Person] = Ordering.by(_.name)
  }

  object AgeOrdering {

    implicit def ageOrdering: Ordering[Person] = Ordering.by(_.age)
  }

  import NameOrdering._
  println(persons.sorted)

  /*
    - totalPrice = most used (50%)
    - by unit count = 25%
    - by unit price = 25%
   */
  case class Purchase(unitCount: Int, unitPrice: Double)

  object Purchase {
    implicit val totalPriceOrdering: Ordering[Purchase] = Ordering.by(x => x.unitCount * x.unitPrice)
  }

  object UnitCountOrdering {
    implicit val unitCountOrdering: Ordering[Purchase] = Ordering.by(_.unitCount)
  }

  object UnitPriceOrdering {
    implicit val unitPriceOrdering: Ordering[Purchase] = Ordering.by(_.unitPrice)
  }
}
