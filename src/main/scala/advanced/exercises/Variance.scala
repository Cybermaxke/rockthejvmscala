package advanced.exercises

object Variance extends App {

  class InvariantParking[T](things: List[T]) {
    def park(vehicle: T): InvariantParking[T] = ???
    def impound(vehicles: List[T]): InvariantParking[T] = ???
    def checkVehicles(conditions: String): List[T] = ???
    def flatMap[R](f: List[T] => InvariantParking[R]): InvariantParking[R] = ???
  }

  class CovariantParking[+T](things: List[T]) {
    def park[B >: T](vehicle: B): CovariantParking[B] = ???
    def impound[B >: T](vehicles: List[B]): CovariantParking[B] = ???
    def checkVehicles(conditions: String): List[T] = ???
    def flatMap[R](f: List[T] => CovariantParking[R]): CovariantParking[R] = ???
  }

  class ContravariantParking[-T](things: List[T]) {
    def park(vehicle: T): ContravariantParking[T] = ???
    def impound(vehicles: List[T]): ContravariantParking[T] = ???
    def checkVehicles[B <: T](conditions: String): List[B] = ???
    def flatMap[B <: T, R](f: List[B] => ContravariantParking[R]): ContravariantParking[R] = ???
  }

  /*
    Rule of thumb:
    - use covariance = collection of things
    - use contravariance = group of actions
   */

  class Vehicle
  class Bike extends Vehicle
  class Car extends Vehicle

  // with e.g. someone else's API: IList[T]

  class IList[T]

  class IInvariantParking[T](things: IList[T]) {
    def park(vehicle: T): InvariantParking[T] = ???
    def impound(vehicles: IList[T]): InvariantParking[T] = ???
    def checkVehicles(conditions: String): IList[T] = ???
  }

  class ICovariantParking[+T](things: IList[T]) {
    def park[B >: T](vehicle: B): CovariantParking[B] = ???
    def impound[B >: T](vehicles: IList[B]): CovariantParking[B] = ???
    def checkVehicles[B >: T](conditions: String): IList[B] = ???
  }

  class IContravariantParking[-T](things: IList[T]) {
    def park(vehicle: T): ContravariantParking[T] = ???
    def impound[B <: T](vehicles: IList[B]): ContravariantParking[T] = ???
    def checkVehicles[B <: T](conditions: String): IList[B] = ???
  }

}
