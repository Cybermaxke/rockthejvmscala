package advanced.exercises

object EqualityPlayground extends App {

  case class User(name: String, age: Int, email: String)
  val john = User("John", 32, "john@rockthejvm.com")

  trait Equal[T] {
    def apply(a: T, b: T): Boolean
  }

  object Equal {

    def apply[T](a: T, b: T)(implicit equality: Equal[T]): Boolean =
      equality(a, b)

    def apply[T](implicit equality: Equal[T]): Equal[T] = equality
  }

  object NameEquality extends Equal[User] {
    override def apply(a: User, b: User): Boolean = a.name == b.name
  }

  implicit object FullEquality extends Equal[User] {
    override def apply(a: User, b: User): Boolean = a.name == b.name && a.email == b.email
  }

  implicit class EqualEnrichment[T](val value: T) extends AnyVal {
    def ===(other: T)(implicit equal: Equal[T]): Boolean =
      equal(value, other)
    def !==(other: T)(implicit equal: Equal[T]): Boolean =
      !equal(value, other)
  }

  println(Equal(john, User("John", 48, "john@rockthejvm.com")))

  println(john === User("John", 48, "john@rockthejvm.com"))
  println(john !== User("John", 48, "john@rockthejvm.com"))
}
