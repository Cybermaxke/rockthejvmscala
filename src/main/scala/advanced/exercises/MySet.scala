package advanced.exercises

import scala.annotation.tailrec

trait MySet[A] extends (A => Boolean) {

  override def apply(element: A): Boolean =
    contains(element)

  def contains(element: A): Boolean
  def +(element: A): MySet[A]
  def ++(anotherSet: MySet[A]): MySet[A]
  def map[B](f: A => B): MySet[B]
  def flatMap[B](f: A => MySet[B]): MySet[B]
  def filter(predicate: A => Boolean): MySet[A]
  def foreach(f: A => Unit)
  def -(element: A): MySet[A]
  def &(anotherSet: MySet[A]): MySet[A]
  def --(anotherSet: MySet[A]): MySet[A]

  // set[1,2,3] =>
  def unary_! : MySet[A]
}

case class EmptySet[A]() extends MySet[A] {
  override def contains(element: A) = false
  override def +(element: A): MySet[A] = NonEmptySet(element, this)
  override def ++(anotherSet: MySet[A]): MySet[A] = anotherSet
  override def map[B](f: A => B): MySet[B] = new EmptySet[B]
  override def flatMap[B](f: A => MySet[B]): MySet[B] = new EmptySet[B]
  override def filter(predicate: A => Boolean): MySet[A] = this
  override def foreach(f: A => Unit): Unit = {}
  override def -(element: A): MySet[A] = this
  override def --(anotherSet: MySet[A]): MySet[A] = anotherSet // difference
  override def &(anotherSet: MySet[A]): MySet[A] = this // intersection
  override def unary_! : MySet[A] = PropertyBasedSet(_ => true)
}

// all elements of type A which satisfy a property
case class PropertyBasedSet[A](property: A => Boolean) extends MySet[A] {

  override def contains(element: A): Boolean =
    property(element)

  override def +(element: A): MySet[A] =
    PropertyBasedSet(x => property(x) || element == x)

  override def ++(anotherSet: MySet[A]): MySet[A] =
    PropertyBasedSet(x => property(x) || anotherSet(x))

  override def map[B](f: A => B): MySet[B] = politelyFail

  override def flatMap[B](f: A => MySet[B]): MySet[B] = politelyFail

  override def filter(predicate: A => Boolean): MySet[A] =
    PropertyBasedSet(x => property(x) && predicate(x))

  override def foreach(f: A => Unit): Unit = politelyFail

  override def -(element: A): MySet[A] =
    filter(element != _)

  override def --(anotherSet: MySet[A]): MySet[A] =
    filter(anotherSet)

  override def &(anotherSet: MySet[A]): MySet[A] =
    filter(!anotherSet)

  override def unary_! : MySet[A] =
    PropertyBasedSet(e => !property(e))

  def politelyFail = throw new IllegalArgumentException("Really deep rabbit hole!")
}

case class NonEmptySet[A](head: A, tail: MySet[A]) extends MySet[A] {

  override def contains(element: A): Boolean =
    head == element || tail.contains(element)

  override def +(element: A): MySet[A] =
    if (contains(element)) this
    else NonEmptySet(element, this)

  override def ++(anotherSet: MySet[A]): MySet[A] = anotherSet match {
    case EmptySet() => this
    case PropertyBasedSet(property) => new PropertyBasedSet[A](x => property(x) || contains(x))
    case NonEmptySet(head, tail) => this + head ++ tail
  }

  override def map[B](f: A => B): MySet[B] =
    tail.map(f) + f(head)

  override def flatMap[B](f: A => MySet[B]): MySet[B] =
    tail.flatMap(f) ++ f(head)

  override def filter(predicate: A => Boolean): MySet[A] =
    if (predicate(head)) tail.filter(predicate) + head
    else tail.filter(predicate)

  override def foreach(f: A => Unit): Unit = {
    f(head)
    tail.foreach(f)
  }

  override def -(element: A): MySet[A] =
    if (element == head) tail
    else {
      val newTail = tail - element
      if (newTail == tail) this // Only construct a new set if the value was actually in the set
      else newTail + head
    }

  override def --(anotherSet: MySet[A]): MySet[A] = anotherSet match {
    case EmptySet() => this
    case NonEmptySet(head, tail) => this - head -- tail
  }

  override def &(anotherSet: MySet[A]): MySet[A] = filter(anotherSet)

  override def unary_! : MySet[A] =
    PropertyBasedSet(x => !contains(x))
}

object MySet {
  def apply[A](values: A*): MySet[A] = {
    @tailrec
    def build(seq: Seq[A], acc: MySet[A]): MySet[A] =
      if (seq.isEmpty) acc
      else build(seq.tail, acc + seq.head)
    build(values.toSeq, new EmptySet[A])
  }
}

object MySetPlayground extends App {
  val s = MySet(1, 2, 3, 4)
  // s + 5 ++ MySet(-2, -1) + 3 map(_ * 3) foreach println
  // s - 1 - 3 foreach println
  // s -- MySet(1, 3) foreach println

  val negative = !s
  println(negative(2))
  println(negative(5))

  val negativeEven = negative.filter(_ % 2 == 0)
  println(negativeEven(5))
  println(negativeEven(6))

  val negativeEven5 = negativeEven + 5
  println(negativeEven5(5))
}