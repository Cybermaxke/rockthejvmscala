package advanced.exercises

object CurriesPAF extends App {

  val simpleAddFunction = (x: Int, y: Int) => x + y
  def simpleAddMethod(x: Int, y: Int) = x + y
  def curriedAddMethod(x: Int)(y: Int) = x + y

  // add7: Int => Int => 7 + y
  // as many different implementations of add7 using the above

  val add7_1: Int => Int = curriedAddMethod(7)
  val add7_2: Int => Int = x => simpleAddFunction(7, x)
  val add7_3: Int => Int = simpleAddFunction(7, _)
  val add7_4: Int => Int = x => simpleAddMethod(7, x)
  val add7_5: Int => Int = simpleAddMethod(7, _)
  val add7_6 = curriedAddMethod(7) _
  val add7_7 = simpleAddMethod(7, _)
  val add7_8 = simpleAddFunction(7, _)

  println(add7_7(8))

  /*
    1. Process a list of numbers and return their string representations with different formats
       Use the %4.2f, %8.6f and %14.12f with a curried formatter function.
   */
  def formatter(format: String)(number: Double) = format.format(number)

  val numbers = List(1.357895, 25413.42, 1239.1477777777752)
  numbers.map(formatter("%4.2f")).foreach(println)
  numbers.map(formatter("%8.6f")).foreach(println)

  /*
    2. different between
       - functions vs methods
       - parameters: by name vs 0-lambda
   */

  def byName(n: => Int) = n + 1
  def byFunction(f: () => Int) = f() + 1

  def method: Int = 42
  def parenMethod(): Int = 42

  /*
    calling byName and byFunction
    - int
    - method
    - parenMethod
    - lambda
    - PAF
   */
  byName(23) // ok
  byName(method) // ok
  byName(parenMethod())
  byName(parenMethod) // ok, but beware ==> byName(parenMethod())
  // byName(() => 42) // not ok
  byName((() => 42)()) // ok
  // byName(parenMethod _) // not ok

  // byFunction(42) // not ok
  // byFunction(method) // not ok
  byFunction(parenMethod) // ok
  byFunction(() => 42) // ok
  byFunction(parenMethod _) // also works
}
