package beginner.lectures

object Generics {

  class Animal
  class Cat extends Animal
  class Dog extends Animal

  class CovariantList[+A] {
    def add[B >: A](element: B): CovariantList[B] = new CovariantList[B]
  }
  val covariantList: CovariantList[Animal] = new CovariantList[Cat]
  val dogList = covariantList.add(new Dog)

  class InvariantList[A]
  val invariantList: InvariantList[Animal] = new InvariantList[Animal]

  class Trainer[-A] // Contravariant
  val trainer: Trainer[Cat] = new Trainer[Animal]

  // bounded
  class Cage[A <: Animal](animal: A)
  val cage = new Cage(new Dog)
}
