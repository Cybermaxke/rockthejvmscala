package beginner.lectures

object StringOps extends App {

  val name = "David"
  val age = 12

  // S-interpolation

  println(s"Hi, my name is $name and I am $age years old.")
  println(s"Hi, my name is $name and I will be turning ${age + 1} years old.")

  // F-interpolation

  val speed = 1.2f
  println(f"$name can eat $speed%2.2f burgers per minute.")

  // Raw-interpolator
  println(raw"This is a \n newline.")
  val escaped = "This is a \n newline."
  println(raw"$escaped")
}
