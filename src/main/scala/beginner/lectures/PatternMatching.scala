package beginner.lectures

import beginner.exercises.MyList
import beginner.exercises.Empty
import beginner.exercises.Cons

object PatternMatching extends App {

  // tuples
  val aTuple = (1,2)
  val matchATuple = aTuple match {
    case (1,1) =>
    case (something, 2) => s"I've found $something"
  }

  val nestedTuple = (1,(2,3))
  val matchNestedTuple = nestedTuple match {
    case (_, (2, v)) => s"$v"
  }

  // list patterns
  val aStandardList = List(1,2,3,42)
  val standardListMatching = aStandardList match {
    case List(1, _, _, _) =>
    case List(1, _*) => // list of arbitrary length
    case 1 :: List(_) => // infix pattern
    case List(1,2,3) :+ 42 => // infix pattern
    case _ => ""
  }

  // type specifiers
  val unknown: Any = 3
  val unknownMatch = unknown match {
    case list: List[Int] => // explicit type specifier
    case _ => ""
  }

  val aList: MyList[Int] = Cons(1, Cons(2, Empty))

  // name binding
  val nameBindingMatch = aList match {
    case nonEmptyList @ Cons(_, _) => // name binding => use the name later (here)
    case Cons(1, rest @ Cons(2, _)) => // name binding inside nested patterns
    case _ => ""
  }

  // multi patterns
  val multiPattern = aList match {
    case Empty | Cons(0, _) => // compound pattern (multi-pattern)
    case _ => ""
  }

  // if guards
  val secondElementSpecial = aList match {
    case Cons(_, Cons(specialElement, _)) if specialElement % 2 == 0 =>
    case _ => ""
  }

  /*
    Question.
   */

  val numbers = List(1,2,3)
  val numbersMatch = numbers match {
    case listOfStrings: List[String] => "a list of strings"
    case listOfNumbers: List[Int] => "a list of numbers"
    case _ => ""
  }

  println(numbersMatch)
}
