package beginner.lectures

import scala.annotation.tailrec

object Recursion extends App {

  // Factorial function 1 * 2 * 3 * .. * n

  // Would fail with big numbers, because new stack frames
  // are created with every recursive call

  def factorial(n: Int): Int = if (n > 1) factorial(n - 1) * n else n

  // Would succeed with big numbers, because old stack frames are
  // replaced by newer ones.
  // -> Tail recursion

  def anotherFactorial(n: Int): BigInt = {
    @tailrec
    def fact(n: Int, acc: BigInt): BigInt = if (n > 1) fact(n - 1, acc * n) else acc
    fact(n, 1)
  }
}
