package beginner.lectures

import scala.annotation.tailrec

object HOFsCurries extends App {

  // function that applies a function n times over a value x
  // nTimes(f, n, x)
  // nTimes(f, 3, x) = f(f(f(x))) = nTimes(f, 2, f(x))
  // nTimes(f, n, x) = f(f(...f(x))) = nTimes(f, n-1, f(x))
  @tailrec
  def nTimes(f: Int => Int, n: Int, x: Int): Int =
    if (n <= 0) x
    else nTimes(f, n - 1, f(x))

  val plus1 = (x: Int) => x + 1
  println(nTimes(plus1, 10, 1))

  def nTimesBetter(f: Int => Int, n: Int): Int => Int =
    if (n <= 0) x => x
    else x => nTimesBetter(f, n - 1)(f(x))

  val plus10 = nTimesBetter(plus1, 10)
  println(plus10(1))

  // curried functions
  val superAdder = (x: Int) => (y: Int) => x + y
  val add3 = superAdder(3)
  println(add3(10))
  println(superAdder(3)(10))

  // functions with multiple parameter lists
  def curriedFormatter(c: String)(x: Double): String = c.format(x)

  val standardFormat: Double => String = curriedFormatter("%4.2f")
  val preciseFormat: Double => String = curriedFormatter("%10.8f")

  println(standardFormat(Math.PI))
  println(preciseFormat(Math.PI))
}
