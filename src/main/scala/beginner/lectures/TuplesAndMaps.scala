package beginner.lectures

object TuplesAndMaps extends App {

  // tuples = finite ordered "lists"
  val aTuple = (2, "hello, Scala") // Tuple2[Int, String] = (Int, String)

  println(aTuple)
  println(aTuple._1) // 2
  println(aTuple.copy(_2 = "goodbye Java"))
  println(aTuple.swap)

  val aMap: Map[String, Int] = Map()

  val phonebook = Map(("Jim", 555), ("Daniel", 789)).withDefaultValue(-1)
  // val phonebook = Map("Jim" -> 555, "Daniel" -> 789)
  println(phonebook)

  // map ops
  println(phonebook.contains("Jim"))
  println(phonebook("Jim"))
  println(phonebook("Mary"))

  // add a pair
  val newPairing = "Mary" -> 678
  val newPhonebook = phonebook + newPairing
  println(newPhonebook)

  // functionals on maps
  println(phonebook.map(pair => pair._1.toLowerCase -> pair._2))
  println(phonebook.view.filterKeys(_.startsWith("J")).toMap)
  println(phonebook.view.mapValues(_ * 10).toMap)

  println(phonebook.toList)
  println(List("Daniel" -> 555).toMap)

  val names = List("Bob", "James", "Angela", "Mary", "Daniel", "Jim")
  println(names.groupBy(_.charAt(0)))
}
