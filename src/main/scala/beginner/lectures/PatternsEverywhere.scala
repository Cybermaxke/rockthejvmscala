package beginner.lectures

object PatternsEverywhere extends App {

  // 1.
  // catches are actual matches
  try {

  } catch {
    case _: RuntimeException => "runtime"
    case _: NullPointerException => "npe"
    case _ => "something else"
  }

  // 2.
  // generators are also based on pattern matching
  val list = List(1,2,3,4)
  val evenOnes = for {
    x <- list if x % 2 == 0
  } yield 10 * x

  val tuples = List((1,2),(3,4))
  val filterTuples = for {
    (first, second) <- tuples
  } yield first * second
  // case classes, :: operators, ... are also available

  // 3.
  val tuple = (1,2,3)
  val (a,b,c) = tuple
  println(b)
  // multiple value definitions based on pattern matching

  val head :: tail = list
  println(head)
  println(tail)

  // 4.
  // partial function
  val mappedList = list.map {
    case v if (v % 2) == 0 => s"$v is even"
    case 1 => "the one"
    case _ => "something else"
  }

  // = equivalent of

  //noinspection MatchToPartialFunction
  val mappedList2 = list.map { x =>
    x match {
      case v if (v % 2) == 0 => v + "is even"
      case 1 => "the one"
      case _ => "something else"
    }
  }

  println(mappedList)
}
