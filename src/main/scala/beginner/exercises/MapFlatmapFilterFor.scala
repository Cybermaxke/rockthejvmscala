package beginner.exercises

object MapFlatmapFilterFor extends App {

  /*
    1. MyList supports for comprehensions?
       map(f: A => B) => MyList[B]
       filter(p: A => Boolean) => MyList[A]
       flatMap(f: A => MyList[B]) => MyList[B]
       foreach(consumer: A => Unit)
    2. A small collection of at most one element - Maybe[+T]
       - map, flatMap, filter
   */

  val list: MyList[Int] = Cons(10, Cons(5, Cons(3, Empty)))

  for {
    v <- list
  } println(v)
}
