package beginner.exercises

import scala.annotation.tailrec

object Functions extends App {

  // 1. A greeting function (name, age) => "Hi, my name is $name and I am $age years old."

  def greeting(name: String, age: Int) = s"Hi, my name is $name and I am $age years old."

  println(greeting("Seppe", 23))

  // 2. Factorial function 1 * 2 * 3 * .. * n

  def factorialFunction(n: Int): Int = if (n > 1) factorialFunction(n - 1) * n else n

  println("factorialFunction(5) = " + factorialFunction(5))

  // 3. A Fibonacci function (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...)
  //    f(1) = 1
  //    f(2) = 1
  //    f(n) = f(n - 1) + f(n - 2)

  def fibonacci(n: Int): Int = if (n == 0) 0 else if (n == 1) 1 else fibonacci(n - 1) + fibonacci(n - 2)

  println("fibonacci(1) = " + fibonacci(1))
  println("fibonacci(2) = " + fibonacci(2))
  println("fibonacci(6) = " + fibonacci(6))

  // 4. Tests if a number is prime.

  def isPrime(n: Int): Boolean = {
    if (n <= 1)
      return false
    @tailrec
    def isPrimeUntil(v: Int): Boolean = if (v < 2) true else n % v != 0 && isPrimeUntil(v - 1)
    isPrimeUntil(n / 2)
  }

  println("isPrime(11) = " + isPrime(11))
  println("isPrime(15) = " + isPrime(15))
  println("isPrime(21) = " + isPrime(21))
  println("isPrime(37) = " + isPrime(37))
}
