package beginner.exercises

import beginner.exercises.Functions.fibonacci

import scala.annotation.tailrec

object Recursion extends App {

  // 1. Concatenate a string n times

  def repeat(s: String, n: Int): String = {
    @tailrec
    def repeatTailrec(v: Int, acc: String): String = if (v > 1) repeatTailrec(v - 1, acc + s) else acc
    repeatTailrec(n, s)
  }

  println(repeat("Test", 3))

  // 2. IsPrime function, tail recursive

  def isPrime(n: Int): Boolean = {
    if (n <= 1)
      return false
    @tailrec
    def isPrimeUntil(v: Int): Boolean = if (v < 2) true else n % v != 0 && isPrimeUntil(v - 1)
    isPrimeUntil(n / 2)
  }

  // 3. Fibonacci function, tail recursive

  def fibonacci(n: Int): Int = {
    @tailrec
    def fibonacciTailrec(i: Int, last: Int, nextToLast: Int): Int =
      if (i >= n) last
      else fibonacciTailrec(i + 1, last + nextToLast, last)

    if (n <= 2) 1 else fibonacciTailrec(2, 1, 1)
  }

  println("fibonacci(1) = " + fibonacci(1))
  println("fibonacci(2) = " + fibonacci(2))
  println("fibonacci(6) = " + fibonacci(6))

}
