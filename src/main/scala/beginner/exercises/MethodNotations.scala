package beginner.exercises

import scala.language.postfixOps

object MethodNotations extends App {

  class Person(val name: String, val age: Int, val favouriteMovie: String) {

    def +(nickname: String) = new Person(s"$name ($nickname)", age, favouriteMovie)
    def unary_+ = new Person(name, age + 1, favouriteMovie)

    def learns(what: String) = s"$name learns $what."
    def learnsScala: String = learns("Scala")

    def apply() = s"Hi, my name is $name and I like $favouriteMovie."
    def apply(n: Int) = s"$name watched $favouriteMovie $n ${if (n == 1) "time" else "times" }."
  }

  /*
    1. Overload the + operator
       mary + "the rockstar" => new person "Mary (the rockstar)"

    2. Add age to Person class
       Add an unary + operator => new person with the age + 1
       +mary

    3. Add a "learns" method in the Person class => "Mary learns Scala"
       Add a learnsScala method, calls learns method with "Scala"
       Use it in postfix notation

    4. Overload the apply method
       mary.apply(2) => "Mary watched Inception 2 times."
   */

  val mary = new Person("Mary", 20, "Inception")
  println(mary())
  println((mary + "the rockstar")())
  println(mary learnsScala)
  println(mary(1))
  println(mary(5))
  println(mary.age)
  println((+mary).age)
}
