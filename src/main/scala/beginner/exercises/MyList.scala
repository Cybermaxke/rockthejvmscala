package beginner.exercises

abstract class MyList[+A] {

  /*
    head = first element of the list
    tail = remainder of the list
    isEmpty = is this list empty
    add(int) => new list with this element added
    toString => a string representation of the list
   */

  def head: A
  def tail: MyList[A]
  def isEmpty: Boolean
  def add[V >: A](value: V): MyList[V]

  def map[B](transformer: A => B): MyList[B]
  def flatMap[B](transformer: A => MyList[B]): MyList[B]
  def filter(filter: A => Boolean): MyList[A]
  def ++[B >: A](list: MyList[B]): MyList[B]
  def sort(comparator: (A, A) => Int): MyList[A]
  def foreach(consumer: A => Unit)
  def zipWith[B, C](list: MyList[B], transformer: (A, B) => C): MyList[C]
  def fold[B](start: B)(transformer: (B, A) => B): B

  def printElements: String

  override def toString: String = "[" + printElements + "]"
}

case object Empty extends MyList[Nothing] {
  override def head: Nothing = throw new NoSuchElementException
  override def tail: MyList[Nothing] = throw new NoSuchElementException
  override def isEmpty: Boolean = true
  override def add[B >: Nothing](value: B): Cons[B] = Cons(value, this)
  override def ++[B >: Nothing](list: MyList[B]): MyList[B] = list
  override def printElements: String = ""
  override def map[B](transformer: Nothing => B): MyList[Nothing] = this
  override def flatMap[B](transformer: Nothing => MyList[B]): MyList[Nothing] = this
  override def filter(filter: Nothing => Boolean): MyList[Nothing] = this
  override def foreach(value: Nothing => Unit): Unit = {}
  override def sort(comparator: (Nothing, Nothing) => Int): MyList[Nothing] = this
  override def zipWith[B, C](list: MyList[B], transformer: (Nothing, B) => C): MyList[Nothing] = this
  override def fold[B](start: B)(transformer: (B, Nothing) => B): B = start
}

case class Cons[T](h: T, t: MyList[T]) extends MyList[T] {
  override def head: T = h

  override def tail: MyList[T] = t

  override def isEmpty: Boolean = false

  override def add[R >: T](value: R): Cons[R] =
    Cons(value, this)

  override def printElements: String =
    h + (if (t.isEmpty) "" else " " + t.printElements)

  override def map[R](transformer: T => R): MyList[R] =
    new Cons[R](transformer(h), t.map(transformer))

  override def flatMap[R](transformer: T => MyList[R]): MyList[R] =
    transformer(h) ++ t.flatMap(transformer)

  override def filter(predicate: T => Boolean): MyList[T] =
    if (predicate(h)) t.filter(predicate) else Cons(h, t.filter(predicate))

  override def ++[B >: T](list: MyList[B]): MyList[B] =
    new Cons[B](h, t ++ list)

  override def foreach(consumer: T => Unit): Unit = {
    consumer(h)
    t.foreach(consumer)
  }

  override def sort(comparator: (T, T) => Int): MyList[T] = {
    if (t.isEmpty) this
    else {
      val tail = t.sort(comparator)
      val c = comparator(h, tail.head)
      if (c > 0) Cons(tail.head, Cons(h, tail.tail).sort(comparator))
      else this
    }
  }

  override def zipWith[B, C](list: MyList[B], transformer: (T, B) => C): MyList[C] =
    if (list.isEmpty) throw new IllegalArgumentException("The lists don't have the same length.")
    else Cons(transformer(h, list.head), tail.zipWith(list.tail, transformer))

  override def fold[B](start: B)(transformer: (B, T) => B): B = tail.fold(transformer(start, h))(transformer)
}

object ListTest extends App {
  val listOfInts = Cons(1, Cons(2, Cons(3, Empty)))
  val anotherListOfInts = Cons(5, Cons(6, Cons(7, Empty)))
  println(listOfInts)
  println(listOfInts.tail.head)
  println(listOfInts.add(4))
  println(listOfInts.map(_ * 2))
  println(listOfInts ++ anotherListOfInts)
  println(listOfInts.flatMap((input: Int) => new Cons(input * 2, new Cons(input * 3, Empty))))
  println("filter: " + listOfInts.filter(_ % 2 == 0))
  println("zipWith: " + listOfInts.zipWith(anotherListOfInts, (x: Int, y: Int) => x + y))
  println("fold: " + listOfInts.fold(0)((x: Int, y: Int) => x + y))
  println("fold2: " + anotherListOfInts.fold(0)((x: Int, y: Int) => x + y))
  listOfInts.foreach((x: Int) => println("forEach: " + x))

  val randomInts = Cons(10, Cons(5, Cons(6, Cons(2, Cons(4, Cons(7, Cons(11, Empty)))))))
  println("sort: " + randomInts + " -> " + randomInts.sort(_ - _))

  val listOfStrings = new Cons("Hello", new Cons("Scala", Empty))
  println(listOfStrings)
  println(listOfStrings.tail.head)
  println(listOfStrings.add(4))
}

/*
  1. Generic trait MyPredicate[-T]
  2. Generic trait MyTransformer[-T, R]
  3. MyList:
     - map(transformer) => MyList
     - filter(predicate) => MyList
     - flatMap(transformer from to MyList[B]) => MyList[B]

     class EvenPredicate extends MyPredicate[Int]
     class StringToIntTransformer extends MyTransformer[String, Int]

     [1,2,3].map(n * 2) = [2,4,6]
 */
