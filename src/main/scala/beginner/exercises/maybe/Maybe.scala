package beginner.exercises.maybe

sealed trait Maybe[+T] {

  def map[R](map: T => R): Maybe[R]

  def flatMap[R](map: T => Maybe[R]): Maybe[R]

  def filter(filter: T => Boolean): Maybe[T]

  def foreach(action: T => Unit)
}

final case class Some[S](value: S) extends Maybe[S] {
  override def map[R](map: S => R): Maybe[R] = Some(map(value))
  override def flatMap[R](map: S => Maybe[R]): Maybe[R] = map(value)
  override def filter(filter: S => Boolean): Maybe[S] = if (filter(value)) this else No
  override def foreach(action: S => Unit): Unit = action(value)
}

case object No extends Maybe[Nothing] {
  override def map[R](map: Nothing => R): Maybe[Nothing] = this
  override def flatMap[R](map: Nothing => Maybe[R]): Maybe[Nothing] = this
  override def filter(filter: Nothing => Boolean): Maybe[Nothing] = this
  override def foreach(action: Nothing => Unit): Unit = {}
}

object MaybeTest extends App {
  val maybeInt: Maybe[Int] = Some(100)
  val maybeAnotherInt: Maybe[Int] = No

  println(maybeInt)
  println(maybeAnotherInt)
  println(maybeInt.map(_ * 10))
  println(maybeAnotherInt.map(_ * 10))
}
