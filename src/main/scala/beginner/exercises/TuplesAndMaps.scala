package beginner.exercises

object TuplesAndMaps extends App {

  /*
    1. What would happen if I had two original entries "Jim" -> 555 and "JIM" -> 900?
    2. Overly simplified social network based on maps
       Person = String
       - add a person to the network
       - remove
       - friend (mutual)
       - unfriend

       - numbers of friends of a person
       - person with most friends
       - how many people have no friends
       - if there is a social connection between two people (direct or not)
   */

  def add(network: Map[String, Set[String]], person: String): Map[String, Set[String]] = network + (person -> Set())

  def remove(network: Map[String, Set[String]], person: String): Map[String, Set[String]] = {
    val friends = network(person)
    val n = friends.foldLeft(network)(removeFriend(_, person, _))
    n - person
  }

  def friend(network: Map[String, Set[String]], personA: String, personB: String): Map[String, Set[String]] = {
    val a = personA -> (network(personA) + personB)
    val b = personB -> (network(personB) + personA)
    network + a + b
  }

  def removeFriend(network: Map[String, Set[String]], personA: String, personB: String): Map[String, Set[String]] = {
    val a = personA -> (network(personA) - personB)
    val b = personB -> (network(personB) - personA)
    network + a + b
  }

  def countFriendsFor(network: Map[String, Set[String]], person: String): Int = network(person).size

  def findPersonWithMostFriends(network: Map[String, Set[String]]): String = network.maxBy(_._2.size)._1

  def countPersonsWithoutFriends(network: Map[String, Set[String]]): Int = network.count(_._2.isEmpty)

  def hasSocialConnection(network: Map[String, Set[String]], personA: String, personB: String): Boolean = {
    val friends = network(personA)
    (friends ++ friends.flatMap(network(_))).contains(personB)
  }

  var network: Map[String, Set[String]] = Map()
  network = add(network, "Jeff")
  network = add(network, "Mary")
  network = add(network, "Dirk")
  network = add(network, "Eve")
  network = friend(network, "Dirk", "Jeff")
  network = friend(network, "Mary", "Jeff")

  println(network)
  println(countFriendsFor(network, "Jeff"))
  println(countPersonsWithoutFriends(network))
  println(hasSocialConnection(network, "Jeff", "Dirk"))
  println(hasSocialConnection(network, "Mary", "Dirk"))
  println(hasSocialConnection(network, "Mary", "Eve"))

  println(findPersonWithMostFriends(network))
  network = removeFriend(network, "Dirk", "Jeff")
  println(countFriendsFor(network, "Jeff"))

  println(hasSocialConnection(network, "Jeff", "Dirk"))
  println(hasSocialConnection(network, "Mary", "Dirk"))
  println(countPersonsWithoutFriends(network))

  network = friend(network, "Dirk", "Jeff")
  println(countPersonsWithoutFriends(network))
  network = remove(network, "Dirk")
  println(countFriendsFor(network, "Jeff"))

}
