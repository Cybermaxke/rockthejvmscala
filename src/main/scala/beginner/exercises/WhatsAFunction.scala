package beginner.exercises

object WhatsAFunction extends App {

  /*
    1. A function which takes 2 strings and concatenates them
    2. Transform the MyPredicate and MyTransformer into function types
    3. Define a function which tales an int and returns another function which tales an int and returns an int
   */

  // 1.
  val concat: (String, String) => String = (v1, v2) => v1 + v2
  println(concat("Hello ", "world"))

  // 3.
  val fn2d: Int => Int => Int = v1 => { v2 => {v1 * v2} }
  println(fn2d(2)(3))
}
