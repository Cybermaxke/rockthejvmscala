package beginner.exercises

object OOBasics extends App {

  /*
    Novel and a Writer

    Writer: first name, surname, year
    - method fullname

    Novel: name, year of release, author
    - authorAge
    - isWrittenBy(author)
    - copy(new year of release)
   */

  val author = new Writer("Charles", "Dickens", 1812)
  val novel = new Novel("Great Expectations", 1861, author)

  println(novel.authorAge)
  println(novel.isWrittenBy(author))

  /*
    Counter class
      - receives an int value
      - method current count
      - method to increment/decrement => new counter
      - overload inc/dec to receive an amount
   */

}

class Writer(val firstName: String,
             val lastName: String,
             val year: Int) {

  def fullName: String = this.firstName + ' ' + this.lastName
}

class Novel(val name: String,
            val yearOfRelease: Int,
            val author: Writer) {

  def authorAge: Int = this.yearOfRelease - this.author.year

  def isWrittenBy(author: Writer): Boolean = this.author == author

  def copy(yearOfRelease: Int = this.yearOfRelease) = new Novel(this.name, yearOfRelease, this.author)
}

class Counter(val count: Int) {

  def getCount: Int = this.count

  def inc: Counter = inc(1)

  def dec: Counter = dec(1)

  def inc(value: Int): Counter = new Counter(this.count + value)

  def dec(value: Int): Counter = new Counter(this.count - value)
}