package beginner.exercises

object PatternMatching extends App {

  /*
    simple function uses pm
    takes an Expr => human readable form

    Sum(Number(2), Number(3)) => 2 + 3
    Sum(Number(2), Number(3), Number(4)) => 2 + 3 + 4
    Prod(Sum(Number(2), Number(3)), Number(4)) => 2 + 3 * 4
    Sum(Prod(Number(2), Number(3)), Number(4)) => 2 * 3 + 4
   */
  trait Expr
  case class Number(n: Int) extends Expr
  case class Sum(e1: Expr, e2: Expr) extends Expr
  case class Prod(e1: Expr, e2: Expr) extends Expr

  def toHumanReadable(expr: Expr): String = {
    expr match {
      case Number(n) => n.toString
      case Sum(e1, e2) => toHumanReadable(e1) + " + " + toHumanReadable(e2)
      case Prod(e1, e2) =>
        def maybeWithParentheses(e: Expr) = e match {
          case Prod(_, _) => toHumanReadable(e)
          case Number(_) => toHumanReadable(e)
          case _ => "(" + toHumanReadable(e) + ")"
        }
        maybeWithParentheses(e1) + " * " + maybeWithParentheses(e2)
    }
  }

  println(toHumanReadable(Sum(Number(2), Number(3))))
  println(toHumanReadable(Sum(Number(2), Sum(Number(3), Number(4)))))
  println(toHumanReadable(Prod(Sum(Number(2), Number(3)), Number(4))))
  println(toHumanReadable(Sum(Prod(Number(2), Number(3)), Number(4))))
}
