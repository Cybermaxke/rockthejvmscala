package filesystem

import java.util.Scanner

import filesystem.command.Command
import filesystem.path.Directory

object Filesystem extends App {

  val root = Directory.root
  var state = State(root, root)
  val scanner = new Scanner(System.in)

  while (true) {
    state.show()

    val input = scanner.nextLine()
    val command = Command.from(input)
    state = command(state)
  }

  /*

  val root = Directory.root
  var state = State(root, root)
  state.show()

  io.Source.stdin.getLines().foldLeft(state) { (state, line) =>
    state.show()
    val command = Command.from(line)
    command(state)
  }

   */
}
