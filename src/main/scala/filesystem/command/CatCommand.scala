package filesystem.command
import filesystem.State
import filesystem.path.File

class CatCommand(name: String) extends Command {

  override def apply(state: State): State = {
    val path = state.resolvePath(name)
    state.root.findDescendant(path).map {
      case file: File => state.withMessage(file.content)
      case _ => state.withMessage(s"$name: isn't a file")
    }.getOrElse(state.withMessage(s"$name: doesn't exist"))
  }
}
