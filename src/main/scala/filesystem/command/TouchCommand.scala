package filesystem.command

class TouchCommand(name: String) extends CreateEntryCommand(name, _.withNewFile(_, "")) {
}
