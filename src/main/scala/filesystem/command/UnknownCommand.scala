package filesystem.command

import filesystem.State

class UnknownCommand extends Command {

  override def apply(state: State): State =
    state.withMessage("Command not found!")
}
