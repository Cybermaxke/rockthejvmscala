package filesystem.command

import filesystem.State
import filesystem.path.Directory

class CdCommand(name: String) extends Command {

  override def apply(state: State): State = {
    val newWd = state.root.findDescendant(state.resolvePath(name))
    newWd.map {
      case dir: Directory => new State(state.root, dir, "")
      case _ => state.withMessage(s"The path doesn't end in a directory: $name")
    }.orElse {
      Some(state.withMessage(s"$name: doesn't exist"))
    }.head
  }
}
