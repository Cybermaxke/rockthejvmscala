package filesystem.command

class MkdirCommand(name: String) extends CreateEntryCommand(name, _.withNewDirectory(_)) {

  override def checkIllegal(name: String): Boolean = {
    name.contains(".")
  }
}
