package filesystem.command
import filesystem.State

class PwdCommand extends Command {

  override def apply(state: State): State = state.withMessage(state.wd.plainPath)
}
