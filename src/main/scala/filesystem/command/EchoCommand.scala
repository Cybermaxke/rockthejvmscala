package filesystem.command
import filesystem.State
import filesystem.path.File

class EchoCommand(args: List[String]) extends Command {

  override def apply(state: State): State = {
    if (args.isEmpty) state
    else if (args.length == 1) state.withMessage(args.head)
    else {
      // Next to last argument
      val operator = args(args.length - 2)

      if (operator == ">" || operator == ">>") {
        val content = args.dropRight(2).mkString(" ")
        val pathName = args.last
        val path = state.resolvePath(pathName)

        // write content
        if (operator == ">") {
          val newRoot = state.root.withNewFile(path, content, overwrite = true)
          state.withRoot(newRoot)
          // append content
        } else {
          state.root.findDescendant(path).map {
            case file: File =>
              val newRoot = state.root.withNewFile(path, file.content + "\n" + content, overwrite = true)
              state.withRoot(newRoot)
            case _ =>
              state.withMessage(s"$pathName: not a file")
          }.getOrElse {
            val newRoot = state.root.withNewFile(path, content, overwrite = true)
            state.withRoot(newRoot)
          }
        }
      } else state.withMessage(args.mkString(" "))
    }
  }
}
