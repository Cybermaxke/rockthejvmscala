package filesystem.command

import filesystem.State
import filesystem.path.Directory

abstract class CreateEntryCommand(name: String, create: (Directory, List[String]) => Directory) extends Command {

  override def apply(state: State): State = {
    val path = state.resolvePath(name)
    if (state.root.contains(path)) {
      state.withMessage(s"Entry $name already exists.")
    } else if (checkIllegal(name)) {
      state.withMessage(s"$name: illegal entry path!")
    } else {
      create(state, path)
    }
  }

  private def create(state: State, relativePath: List[String]): State = {
    val wd = state.wd

    // 1. create path for the new entry from the root of the structure
    val newPath = wd.path ++ relativePath

    // 2. create a new entry within the structure
    val newRoot = create(state.root, newPath)

    // 3. find new working directory instance given path
    val newWd = newRoot.findDescendant(wd.path).head.asDirectory

    State(newRoot, newWd)
  }

  def checkIllegal(name: String): Boolean = false
}
