package filesystem.command

import filesystem.State

trait Command extends (State => State)

object Command {

  private val mkdir = "mkdir"
  private val ls = "ls"
  private val pwd = "pwd"
  private val touch = "touch"
  private val cd = "cd"
  private val rm = "rm"
  private val echo = "echo"
  private val cat = "cat"

  def emptyCommand(): Command = (state: State) => state

  def incompleteCommand(name: String): Command = (state: State) => state.withMessage(name + ": incomplete command!")

  def from(input: String): Command = {
    if (input.isEmpty) emptyCommand()
    else {
      val tokens = input.split(" ")
      val name = tokens(0)
      if (mkdir == name) {
        if (tokens.length < 2) incompleteCommand(mkdir)
        else new MkdirCommand(tokens(1))
      } else if (ls == name) {
        new LsCommand
      } else if (pwd == name) {
        new PwdCommand
      } else if (touch == name) {
        if (tokens.length < 2) incompleteCommand(touch)
        else new TouchCommand(tokens(1))
      } else if (cd == name) {
        if (tokens.length < 2) incompleteCommand(cd)
        else new CdCommand(tokens(1))
      } else if (rm == name) {
        if (tokens.length < 2) incompleteCommand(rm)
        else new RmCommand(tokens(1))
      } else if (echo == name) {
        if (tokens.length < 2) incompleteCommand(echo)
        else new EchoCommand(tokens.tail.toList)
      } else if (cat == name) {
        if (tokens.length < 2) incompleteCommand(cat)
        else new CatCommand(tokens(1))
    }
      else new UnknownCommand
    }
  }
}