package filesystem.command
import filesystem.State
import filesystem.path.{DirEntry, Directory}

class LsCommand extends Command {

  override def apply(state: State): State = {
    val children = state.wd.children
    state.withMessage(generateOutput(children))
  }

  private def generateOutput(contents: List[DirEntry]): String = {
    contents.foldLeft("")((s, entry) => {
      val entryType = entry match {
        case _: Directory => "Directory"
        case _ => "File"
      }
      s + (if (s.isEmpty) "" else "\n") + entry.name + '[' + entryType + "]"
    })
  }
}
