package filesystem.command
import filesystem.State

class RmCommand(name: String) extends Command {

  override def apply(state: State): State = {
    val path = state.resolvePath(name)

    state.root.findDescendant(path).map { entry =>
      if (entry == state.root) {
        state.withMessage("Removing the root directory isn't supported!")
      } else {
        val newRoot = state.root.withoutEntry(path)
        state.withRoot(newRoot).withMessage(s"$name: successfully removed")
      }
    }.getOrElse(state.withMessage(s"$name: doesn't exist"))
  }
}
