package filesystem.path

import java.nio.file.FileSystemException

//noinspection NameBooleanParameters
class Directory(override val path: List[String],
                val children: List[DirEntry])
  extends DirEntry(path) {

  def findChild(name: String): Option[DirEntry] = this.children.find(_.name == name)

  def findDescendant(path: List[String]): Option[DirEntry] = {
    if (path.isEmpty) {
      Some(this)
    } else {
      val name = path.head
      val childPath = path.drop(1)
      children.find(_.name == name).flatMap(entry => {
        if (childPath.isEmpty) Some(entry)
        else {
          entry match {
            case dir: Directory => dir.findDescendant(childPath)
            case _ => None
          }
        }
      })
    }
  }

  def withNewDirectory(path: List[String]): Directory = withNewEntry(path)(path => Directory.empty(path))

  def withNewDirectory(name: String): Directory = withNewEntryByName(name)(path => Directory.empty(path))

  def withNewFile(path: List[String], content: String): Directory = withNewFile(path, content, overwrite = false)

  def withNewFile(path: List[String], content: String, overwrite: Boolean): Directory =
    withNewEntry(path, overwrite)(path => new File(path, content))

  def withNewFile(path: String, content: String): Directory = withNewFile(path, content, overwrite = false)

  def withNewFile(name: String, content: String, overwrite: Boolean): Directory =
    withNewEntryByName(name, overwrite)(path => new File(path, content))

  private def withNewEntry[E <: DirEntry](path: List[String], overwrite: Boolean = false)(builder: List[String] => E): Directory = {
    if (path.isEmpty) {
      this
    } else {
      val name = path.head
      val childPath = path.drop(1)
      if (childPath.isEmpty) {
        withNewEntryByName(name, overwrite)(builder)
      } else {
        children.find(_.name == name)
          .map {
            case entry@(dir: Directory) => new Directory(this.path,
              // Replace the child entry in the children
              children.patch(children.indexOf(entry), Seq(dir.withNewEntry(childPath, overwrite)(builder)), 1))
            case _ => throw new IllegalArgumentException(
              "Cannot create an entry in a file.")
          }
          // The directory doesn't exist yet, create it and try to create
          // the entry again.
          .getOrElse(withNewDirectory(name).withNewEntry(path, overwrite)(builder))
      }
    }
  }

  private def withNewEntryByName[E <: DirEntry](name: String, overwrite: Boolean = false)(builder: List[String] => E): Directory = {
    val c = findChild(name).map { entry =>
      if (overwrite) children.filter(_ != entry)
      else throw new IllegalArgumentException(s"There's already an entry with the given name: $name")
    }.getOrElse(children)
    val entry = builder(path :+ name)
    new Directory(path, c :+ entry)
  }

  def withoutEntry(path: List[String]): Directory = {
    val name = path.head
    val childPath = path.drop(1)
    if (childPath.isEmpty) {
      withoutEntry(name)
    } else {
      children.find(_.name == name)
        .map {
          case entry@(dir: Directory) => new Directory(this.path,
            children.patch(children.indexOf(entry), Seq(dir.withoutEntry(childPath)), 1))
          case _ => this
        }
        .getOrElse(this)
    }
  }

  def withoutEntry(name: String): Directory = {
    if (!contains(name)) this
    else new Directory(path, children.filter(_.name != name))
  }

  def contains(path: List[String]): Boolean = findDescendant(path).isDefined

  def contains(name: String): Boolean = findChild(name).isDefined

  override def asDirectory: Directory = this

  override def asFile: File =
    throw new FileSystemException("Directory cannot be converted to a file.")

  override def toString: String = s"Directory(path=$path,children=$children)"
}

object Directory {

  def root: Directory = new Directory(List(), List())

  def empty(path: List[String]) = new Directory(path, List())
}
