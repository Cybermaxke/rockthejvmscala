package filesystem.path

final class ParsedPath(val path: List[String], val relative: Boolean)

object PathParser {

  val SEPARATOR = "/"

  def parse(path: String): ParsedPath = {
    if (path == ".") {
      new ParsedPath(List(), true)
    } else if (path.startsWith("." + SEPARATOR)) {
      new ParsedPath(pathToList(path.substring(("." + SEPARATOR).length)), true)
    } else if (path.startsWith(SEPARATOR)) {
      new ParsedPath(pathToList(path.substring(SEPARATOR.length)), false)
    } else {
      new ParsedPath(pathToList(path), true)
    }
  }

  private def pathToList(path: String): List[String] =
    collapseDotDot(path.split(SEPARATOR).toList.filter(s => !s.isEmpty && s != "."))

  // Collapse paths like /a/.., which is the same as /
  // or a/b/../c -> a/c
  private def collapseDotDot(path: List[String]): List[String] = {
    if (path.isEmpty || path.tail.isEmpty) path
    else if (path.tail.head == ".." && path.head != "..") collapseDotDot(path.tail.tail)
    else path.head +: collapseDotDot(path.tail)
  }
}
