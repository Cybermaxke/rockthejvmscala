package filesystem.path

abstract class DirEntry(val path: List[String]) {

  val name: String = if (path.isEmpty) "" else path.last

  val parentPath: List[String] = path.dropRight(1)

  val plainPath: String = PathParser.SEPARATOR + path.mkString(PathParser.SEPARATOR)

  def asDirectory: Directory

  def asFile: File
}
