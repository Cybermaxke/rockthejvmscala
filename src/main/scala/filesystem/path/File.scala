package filesystem.path

import java.nio.file.FileSystemException

class File(override val path: List[String], val content: String) extends DirEntry(path) {

  override def asDirectory: Directory =
    throw new FileSystemException("File cannot be converted to a directory.")

  override def asFile: File = this

  override def toString: String = s"File(path=$path,content=$content)"
}

object File {

  def empty(path: List[String]) = new File(path, "")
}
