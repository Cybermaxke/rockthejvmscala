package filesystem

import filesystem.path.{Directory, ParsedPath, PathParser}

class State(val root: Directory, val wd: Directory, val output: String) {

  def show(): Unit = {
    if (!output.isEmpty)
      println(output)
    print(wd.plainPath + State.SHELL_TOKEN)
  }

  def resolvePath(path: ParsedPath): List[String] = {
    if (path.relative) {
      val stepsUp = path.path.takeWhile(_ == "..").length
      val steppedUpPath = wd.path.dropRight(stepsUp)
      steppedUpPath ++ path.path.drop(stepsUp)
    } else path.path
  }

  def resolvePath(path: String): List[String] =
    resolvePath(PathParser.parse(path))

  def withMessage(message: String): State =
    State(root, wd, message)

  def withRoot(root: Directory): State = {
    def findWd(path: List[String]): Directory = {
      root.findDescendant(path).map {
        case directory: Directory => directory
        case _ =>
          if (path.isEmpty) root
          else findWd(path.dropRight(1))
      }
      }.getOrElse(root)
    new State(root, findWd(wd.path), "")
  }
}

object State {

  val SHELL_TOKEN = "$ "

  def apply(root: Directory, wd: Directory, output: String = "") =
    new State(root, wd, output)
}